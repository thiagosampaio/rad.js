/**
 * Menu Left
 * @type {Element}
 */

const _likn = 'EVDU6CUZFK6T';
const mdc_drawer = document.querySelector('.mdc-drawer');
const mdc_progress = document.querySelector('.mdc-linear-progress');
const mdc_text_field_icon = document.querySelector('.mdc-text-field-icon');
const selector_field = '.mdc-text-field';
const text_fields = [].map.call(document.querySelectorAll(selector_field), function(el) {
    // return new mdc.ripple.MDCRipple(el);
    return new mdc.textField.MDCTextField(el);
});
const text_fields_icons = [].map.call(document.querySelectorAll('.mdc-text-field-icon'), function(el) {
    // return new mdc.ripple.MDCRipple(el);
    return new mdc.textField.MDCTextFieldIcon(el);
});
// new mdc.checkbox.MDCC






/**
 * Tab Bar Scroller
 */
const mdc_tab_scroller = document.querySelector('.mdc-tab-bar');
const mdc_tab_scroller_contents = document.querySelectorAll('.content');


function openModal() {
    document.querySelector('.mdc-drawer').setAttribute('class', 'mdc-drawer mdc-drawer--modal mdc-drawer--open');
}
function closeModal() {
    document.querySelector('.mdc-drawer').setAttribute('class', 'mdc-drawer mdc-drawer--modal');
}

const selector = '' +
    '.mdc-button, ' +
    '.mdc-top-app-bar,' +
    '.mdc-card,' +
    '.mdc-tab,' +
    '.mdc-fab,' +
    '.mdc-list-item';
const ripples = [].map.call(document.querySelectorAll(selector), function(el) {
    return new mdc.ripple.MDCRipple(el);
});

const top_app_bar = document.querySelector('.mdc-top-app-bar');
const topAppBar = new mdc.topAppBar.MDCTopAppBar(top_app_bar);

if(mdc_tab_scroller) {
    const mdcTabBar = new mdc.tabBar.MDCTabBar(mdc_tab_scroller);
    mdcTabBar.preventDefaultOnClick = true;
    mdcTabBar.listen('MDCTabBar:activated', function(event) {
        document.querySelector('.content--active').classList.remove('content--active');
        // Show content for newly-activated tab
        mdc_tab_scroller_contents[event.detail.index].classList.add('content--active');
    })
}


if(mdc_progress) {

    new mdc.linearProgress.MDCLinearProgress(mdc_progress);
}
const mdcDrawer = new mdc.drawer.MDCDrawer(mdc_drawer);


function limpar() {
    const text_fields_icons = [].map.call(document.querySelectorAll('.mdc-text-field input'), function(el) {
        // return new mdc.ripple.MDCRipple(el);
        console.log(el.value);
        el.value = "";
        // return new mdc.textField.MDCTextFieldIcon(el);
    });
}

function searchGifs_d(value) {
    // EVDU6CUZFK6T
    if(value == "") {
        getTrendingGifs();
    }else {
        loading();
        let url = "https://g.tenor.com/v1/random?q="+value+"&key="+_likn+"&limit=25"
        fetch(url)
        .then(function(response) {
            return response.json();
        })
        .then(function(jsonResponse) {
            // do something with jsonResponse
            console.log(jsonResponse);
            let json = "";
            let json_pars = jsonResponse.results;
            for(let i = 0; i < json_pars.length; i++) {
                json += '<div class="col-6 text-center mdc-card mb-1 pl-1 pr-1" onclick=clickLinkGift(\''+json_pars[i].id+'\')>';
                json += '<div class="mdc-card__content">';
                json += '<div class="mdc-card__primary-action">';
                json += "<div class=\"mdc-card__ripple\"></div>";
                json += '<img class="img-fluid" src="' +json_pars[i].media[0].tinygif.url +'" width="' +json_pars[i].media[0].gif.dims[0] +'" height="' +json_pars[i].media[0].gif.dims[1] +'">';
                json += '</div>';
                json += '</div>';
                json += '</div>';

            }
            json += '<div class="col-6 text-center mb-1">';
            json += '<button class="mdc-button mdc-button--raised">';
                json += '<span class="mdc-button__ripple"></span>';
                json += '<i class="material-icons">add</i>';
                json += '<span class="mdc-button__label">Mais</i>';
            json += '</button>';
            json += '</div>';
            // console.log(json);
            if(document.querySelector('.gifs-results')){
                document.querySelector('.gifs-results').innerHTML = json;
            }
        });
    }
    

}

function getTrendingGifs() {
    
    let url = "https://g.tenor.com/v1/categories?key="+_likn+"&limit=25&locale=pt_BR"
    fetch(url)
    .then(function(response) {
        return response.json();
    })
    .then(function(jsonResponse) {
        // do something with jsonResponse
        console.log(jsonResponse);
        let json = "";
        let json_pars = jsonResponse.tags;
        for(let i = 0; i < json_pars.length; i++) {
            json += '<div class="col-6 mdc-card text-center pt-5 pb-5 mb-1 pl-1 pr-1" style="background-image:url('+json_pars[i].image+'); background-position: center; background-repeat: no-repeat; background-size: cover; ">';
            json += '<div class="mdc-card__content">';
            json += '<div class="mdc-card__primary-action">';
            json += "<div class=\"mdc-card__ripple\"></div>";
            json += '<div class="bg-opacity"></div>';
            json += '<p class="text-center mdc-typography--headline6 text-light text-opacity" onclick="clickLinkTrend(\''+json_pars[i].searchterm+'\')">'+json_pars[i].searchterm+'</p>';
            json += '</div>';
            json += '</div>';
            json += '</div>';

        }
        
        if(document.querySelector('.gifs-results')){
            document.querySelector('.gifs-results').innerHTML = json;
        }
    });

}

function searchGifs_id(value) {
    // EVDU6CUZFK6T
    if(value == "") {
        getTrendingGifs();
    }else {
        loading();
        let url = "https://g.tenor.com/v1/gifs?ids="+value+"&key=EVDU6CUZFK6T&limit=25"
        fetch(url)
        .then(function(response) {
            return response.json();
        })
        .then(function(jsonResponse) {
            // do something with jsonResponse
            console.log(jsonResponse);
            let json = "";
            let json_pars = jsonResponse.results;
            json += '<div class="col-12 text-center mb-1 pl-1 pr-1" onclick=getTrendingGifs()>';
            json += '<button class="mdc-button btn-block mdc-button--raised">';
                json += '<span class="mdc-button__ripple"></span>';
                json += '<i class="material-icons">arrow_back</i>';
                json += '<span class="mdc-button__label"></i>';
            json += '</button>';
            json += '</div>';
            for(let i = 0; i < json_pars.length; i++) {
                json += '<div class="col-12 text-center mb-1">';
                json += '<div class="mdc-card__content">';
                json += '<div class="mdc-card__primary-action">';
                json += "<div class=\"mdc-card__ripple\"></div>";

                json += '<img class="img-fluid" src="' +json_pars[i].media[0].gif.url +'" width="' +json_pars[i].media[0].gif.dims[0] +'" height="' +json_pars[i].media[0].gif.dims[1] +'">';
                json += '</div>';
                json += '</div>';
                json += '</div>';

                json += '<div class="col-12 text-center mb-1">';
                json += '<button class="mdc-button mdc-button--raised" onclick="shareGift(\'enviado por thiago sampaio\', \'Testo teste\', \''+json_pars[i].media[0].gif.url+'\')">';
                    json += '<span class="mdc-button__ripple"></span>';
                    json += '<i class="material-icons">share</i>';
                    json += '<span class="mdc-button__label"></i>';
                json += '</button>';
                json += '</div>';
            }
            
            // console.log(json);
            document.querySelector('.gifs-results').innerHTML = json;
        });
    }
    

}

function clickLinkTrend(vl) {
    document.querySelector('#gifs_text').value = String(vl);
    searchGifs_d(String(vl));
}

function clickLinkGift(vl) {
    document.querySelector('#gifs_text').value = String(vl);
    searchGifs_id(String(vl));
}

function loading() {
    /*document.addEventListener("DOMContentLoaded", function (event) {
        //ready
        console.log(event);
        var elemento = document.querySelector('.loadding').remove();
    });*/
    return '<div class="hidden" id="spinner"></div>';
}

function shareGift (_title, _text, _url) {
    const shareData = {
      title: _title,
      text: _title,
      url: _url,
    }

    
    const resultPara = document.querySelector('.gifs-results');
    // navigator.share(shareData)

    if (navigator.share !== undefined) {
        navigator.share(shareData)
        .then(() => alert('Successful share'))
        .catch((error) => alert('Error sharing: ' +error));
    }
    // Deve ser acionado algum tipo de "ativação do usuário"
    /*btn.addEventListener('click', async () => {
      try {
        
      } catch(err) {
        resultPara.textContent = 'Error: ' + e
      }
      resultPara.textContent = 'MDN compartilhado com sucesso!'
    });*/
}

getTrendingGifs();

function actOnDrop(id) {
    var element = document.getElementById(id);
    if(element.classList == "backdrop-close") {
        element.classList.add("backdrop");
        element.classList.remove("backdrop-close");
    }else {
        element.classList.add("backdrop-close");
        element.classList.remove("backdrop");
    }
    
}
