import { scripts_default } from "./../modules/defaults_scripts/default_scripts.js";
import './../modules/views/example/layout/menu_left.js';
import './../modules/views/example/layout/nav_top.js';
import './../modules/views/example/layout/tab_bar.js';
import '../modules/views/example/dashboard/content.js';
import '../modules/views/example/dashboard/carros.js';
import '../modules/views/example/dashboard/listas.js';
import '../modules/views/example/dashboard/aluguel.js';
import '../modules/views/example/dashboard/chips.js';
import '../modules/views/example/dashboard/functions.js'
import './../modules/views/example/layout/footer.js';

scripts_default(
    [
        // "https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js",
        "js/functions.js",
        'js/modules/views/example/dashboard/functions.js'
    ]
)
