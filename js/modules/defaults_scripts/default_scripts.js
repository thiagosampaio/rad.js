export default class init_default {

}

export function script_modules(defaults = Array()) {
    let df = "";
    for (let i = 0; i < defaults.length; i++) {
        // df += "<script type='module' src=\"" + defaults[i] + "\"></script>";
        let script = document.createElement('script');
        script.src = defaults[i];
        // script.async = true;
        script.type = "module";
        // console.log(script);
        // document.head.appendChild(script);
        document.head.appendChild(script);
    }
}

export function scripts_default(defaults = Array()) {
    let df = "";
    for (let i = 0; i < defaults.length; i++) {
        // df += "<script type='module' src=\"" + defaults[i] + "\"></script>";
        let script = document.createElement('script');
        script.src = defaults[i];
        script.async = false;
        script.type = "application/javascript";
        // document.head.appendChild(script);
        document.head.appendChild(script);
    }
}
