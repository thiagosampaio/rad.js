import { script_modules } from './../defaults_scripts/default_scripts.js'
import { alertError } from './../components.js'
export class init_pages {
    
    constructor(get_route) {
        this.routes = get_route;
    }
    
}

export function set_routes(get_route) {
    let _protocoll = window.location.protocol;
    let _host = window.location.host;
    let _pathname = window.location.pathname;

    // status_file = get_files(_protocoll + "//" + _host + _pathname + "/../js/routes/" + get_route + '.js');
    // console.log(status_file);
    /*script_modules([
        "/../js/routes/" + get_route + '.js'
    ]);*/

    return "location.href='?route="+get_route+"'";
}

export function routes(get_route) {

    let _protocoll = window.location.protocol;
    let _host = window.location.host;
    let _pathname = window.location.pathname;
    let status_file = "";

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    let url_params = urlParams.get('route');
    // console.log(urlParams.get('route'));

    if (url_params) {

        status_file = get_files(_protocoll + "//" + _host + _pathname + "/../js/routes/" + url_params + '.js');
        // console.log(status_file);
        script_modules([
            _protocoll + "//" + _host + _pathname + "/../js/routes/" + url_params + '.js'
        ]);
        // console.log(url_params);
        if (status_file === false) {
            // document.write('Desculpe ocorreu um error ao carregar o arquivo, verifique a rota ou se o arquivo existe.');
            // create_alert.error('Route: 404', 'Desculpe ocorreu um error ao carregar o arquivo, verifique a rota ou se o arquivo existe.' + " <strong style='color: #f60f3c'>" + url_params + ".js</strong>");
            /* navigator.notification.alert(
                    'Desculpe ocorreu um error ao carregar o arquivo, verifique a rota ou se o arquivo existe.',  // message
                    () => {},         // callback
                    'Route: 404',            // title
                    'Ok'                  // buttonName
                );*/
        } else {

        }

    } else {
        script_modules([
            "js/routes/splash.js"
        ]);
    }

    if (urlParams.get('method') !== null) {
        // status_file = init_pages.get_files(_protocoll + "//" + _host + _pathname + "/../js/routes/" + urlParams.get('method') + '.js');
        console.log(status_file);
        console.log(urlParams.get('method'));
    }
}

function get_files(url) {
    try{
        let http = new XMLHttpRequest();
        http.open('GET', url);
        http.onreadystatechange = function (oEvent) {
            if (http.readyState === 4) {
                if (http.status === 200) {
                  // console.log(http.responseText)
                } else {
                   console.log("Error", http.statusText, url);
                   alertError('Error ' + http.statusText + "<br> ROTA: " + url);
                }
            }
        };
        http.send();
        return http.status !== 404;
        
    }catch(e) {
        console.log(e);
        alertError(e);
    }
}

export function getUrlParametros () {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    return urlParams.get('route')
};
