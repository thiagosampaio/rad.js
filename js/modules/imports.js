// document.addEventListener("deviceready", onDeviceReady, false);
/**
 *
 * @type {{
 *  route_scripts: imports.route_scripts,
 *  defaults_metatags: (function(*=): string),
 *  defaults_title: (function(*): string),
 *  defaults_links: (function(*=): string),
 *  defaults_scripts: imports.defaults_scripts
 * }}
 */
imports = {
    /**
     *
     * @param defaults
     */
    defaults_scripts: function (defaults = Array()) {
        let df = "";
        for (let i = 0; i < defaults.length; i++) {
            // df += "<script type='application/javascript' src=\"" + defaults[i] + "\"></script>";
            let script = document.createElement('script');
            script.src = defaults[i];
            script.async = true;
            // document.head.appendChild(script);
            document.head.appendChild(script);
        }
        // document.write(df);
    },
    /**
     *
     * @param defaults
     */
    route_scripts: function (defaults = Array()) {
        // let df = "";

        for (let i = 0; i < defaults.length; i++) {
            // df += "<script type='application/javascript' src=\"" + defaults[i] + ".js\"></script>";
            let script = document.createElement('script');
            script.src = defaults[i] + ".js";
            script.async = true;
            document.body.appendChild(script);
        }

    },
    /**
     *
     * @param defaults
     * @returns {string}
     */
    defaults_links: function (defaults = Array()) {
        let df = "";
        for (let i = 0; i < defaults.length; i++) {
            df += "<link href=\"" + defaults[i] + "\" rel=\"stylesheet\"/>";
        }
        return df;
    },
    /**
     *
     * @param defaults
     * @returns {string}
     */
    defaults_title: function (defaults) {

        return "<title>" + defaults + "</title>";
    },
    /**
     *
     * @param defaults
     * @returns {string}
     */
    defaults_metatags: function (defaults = Array()) {
        let df = "";
        for (let i = 0; i < defaults.length; i++) {
            df += "<meta " + defaults[i] + "/>";
        }
        return df;
    }
}

/**
 *
 * @type {{header: init_header.header}}
 */
init_header = {
    /**
     *
     * @param dt_array
     */
    header: function (dt_array = Array()) {
        let dt = "";
        for (let i = 0; i < dt_array.length; i++) {
            dt += dt_array[i];
        }
        document.getElementById('header').innerHTML = dt;

    }
}
/**
 *
 * @type {{defaults: init_config.defaults}}
 */
init_config = {
    /**
     *
     * @param tema_icons
     * @param tema_layout
     */
    defaults: function (tema_icons = null, tema_layout = null) {


    }
}
/**
 *
 * @type {{scripts: init_scripts.scripts}}
 */
init_scripts = {
    /**
     *
     * @param dt_array
     */
    scripts: function (dt_array = Array()) {
        let dt = "";
        for (let i = 0; i < dt_array.length; i++) {
            dt += dt_array[i];
        }

        document.getElementById('script_load').innerHTML += dt;
    }
}
/**
 *
 * @type {{routes: init_pages.routes, getUrlParametros: (function(): string), get_files: (function(*=): boolean)}}
 */
init_pages = {
    /**
     *
     */
    routes: function (get_route = null) {
        let _protocoll = window.location.protocol;
        let _host = window.location.host;
        let _pathname = window.location.pathname;

        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        let url_params = urlParams.get('route');
        

        if (url_params) {

            status_file = init_pages.get_files(_protocoll + "//" + _host + _pathname + "/../js/routes/" + url_params + '.js');
            // console.log(status_file);
            init_scripts.scripts(
                imports.route_scripts([
                    _protocoll + "//" + _host + _pathname + "/../js/routes/" + url_params
                ])
            );
            // console.log(url_params);
            if (status_file === false) {
                // document.write('Desculpe ocorreu um error ao carregar o arquivo, verifique a rota ou se o arquivo existe.');
                // create_alert.error('Route: 404', 'Desculpe ocorreu um error ao carregar o arquivo, verifique a rota ou se o arquivo existe.' + " <strong style='color: #f60f3c'>" + url_params + ".js</strong>");
                /* navigator.notification.alert(
                     'Desculpe ocorreu um error ao carregar o arquivo, verifique a rota ou se o arquivo existe.',  // message
                     () => {},         // callback
                     'Route: 404',            // title
                     'Ok'                  // buttonName
                 );*/
            } else {

            }

        } else {
            init_scripts.scripts(
                imports.route_scripts([
                    _protocoll + "//" + _host + _pathname + "/../js/routes/" + url_params
                ])
            );
        }

        if (urlParams.get('method') !== null) {
            // status_file = init_pages.get_files(_protocoll + "//" + _host + _pathname + "/../js/routes/" + urlParams.get('method') + '.js');
            console.log(status_file);
            console.log(urlParams.get('method'));
        }


    },
    /**
     *
     * @returns {string}
     */
    getUrlParametros: function () {
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        return urlParams.get('route')
    },
    /**
     *
     * @param url
     * @returns {boolean}
     */
    get_files: function (url) {
        let http = new XMLHttpRequest();
        http.open('GET', url);
        http.send();
        return http.status !== 404;
    }
}
/**
 *
 * @type {{open_route: routes.open_route}}
 */
routes = {
    /**
     *
     * @param params
     */
    open_route: function (params) {
        location.href = "?route=" + params;
    }
}

