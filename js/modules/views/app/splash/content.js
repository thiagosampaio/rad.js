import {
    new_component_card as card,
    new_component_card_primary_action as card_primary_action,
    new_component_card_primary as card_primary,
    new_component_card_media as card_media,
    new_component_grid as grid,
    new_component_div as div,
    new_component_main as main,
    new_component_text as text,
    new_component_card_secundary as card_secondary,
    new_component_card_actions as card_actions,
    new_component_linear_progress as progress_bar,
    new_component_text_field as text_field,
    new_component_img as img,
    new_component_btn,
    new_component_button,
    new_component_linear_progress as linear_progress,
    new_component_grid_cell as grid_cell, getRouteOnclick,
    new_component_float_actions as float_actions,
    new_timeStamp,
    new_md5
} from './../../../components.js';

export function content() {
    // console.log(new_timeStamp(), new_md5(new_timeStamp() + "10e72ba2e4df72fb4323c40ed7ca72ddb509f41c"+"e91bdb517e802244d8e3ce41525ba515"));
    let inner = "";
    /**
     * Progress Bar
     * @type {string}
     */
    inner += linear_progress();

    inner += div([
        div([], 'bg-red'),
        div([]),
        div([
            div([
                text('h1', 'SLOTS', ' text-center text-white font-weight-bolder font-marvel'),
                text('h3', 'CASSINOS', ' text-center text-white font-weight-bolder font-marvel-ligth'),
                text('p', 'Ganhe agora, faça seu login ou cadastre-se', ' text-center text-white font-marvel-ligth'),
                // new_component_btn('idteste', 'button', 'Next', 'arrow_forward','mdc-button--raised mdc-button', 'onclick='+getRouteOnclick('login')+''),
                float_actions('arrow_forward', 'NEXT', 'onclick="'+getRouteOnclick('app/login')+'"'),
            ], 'col'),

        ], 'text-center mt-0'),
        

    ], 'splash-front text-center bg-01 pt-2');

    return inner;
}

document.getElementById('app').innerHTML += content();