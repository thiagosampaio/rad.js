import {
    new_component_card as card,
    new_component_card_primary_action as card_primary_action,
    new_component_card_primary as card_primary,
    new_component_card_media as card_media,
    new_component_grid as grid,
    new_component_div as div,
    new_component_main as main,
    new_component_text as text,
    new_component_card_secundary as card_secondary,
    new_component_card_actions as card_actions,
    new_component_linear_progress as progress_bar,
    new_component_float_actions as float_actions, new_component_grid_cell as grid_cell,
    new_component_tab_panel as tab_panel,
    new_component_img as img,
    new_component_link as link,
    new_component_btn as button,
    new_component_text_field as text_field,
    loading,
    fetchGet,
    render,
} from './../../../components.js'

export function content() {
    let inner = "";

    inner += div([
        // float_actions('arrow_forward', 'next'),
        div([
            div([
                tab_panel([
                    text('h3', 'CARACTERS', 'font-marvel mt-3 mb-3 inlineModuleHeader__header text-danger text-left'),

                ], 'listas', 'content--active'),
                tab_panel([
                    text('h3', 'Chips', 'mdc-typography--headline6 text-center'),
                ], 'chips'),
                tab_panel([
                    text('h3', 'Casas', 'mdc-typography--headline6 text-center'),
                    // text('span', 'image', 'material-icons text-center'),
                    // text('hr', '', ''),
                    div([
                        div([
                            div([
                                img('https://images.pexels.com/photos/255379/pexels-photo-255379.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', 'img-size-100', '', '', ''),
                                text('small', 'Este é mais um teste de texto simples. com bootstrap e material', 'text-danger'),
                                text('br', '', ''),
                                link('Link de teste', '?', 'text-link')
                            ], 'col-6 mt-1'),
                            div([
                                img('https://images.pexels.com/photos/255379/pexels-photo-255379.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', 'img-size-100', '', '', ''),
                                button('btnlink', 'button', 'Adicionar', 'add', 'mdc-button mdc-button--outlined mt-1'),
                                button('btnlink2', 'button', 'Remover', 'remove', 'mdc-button mdc-button--outlined mt-1'),
                                
                            ], 'col-6 mt-1'),
                            div([
                                img('https://images.pexels.com/photos/255379/pexels-photo-255379.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', 'img-size-100', '', '', ''),
                                text('small', 'Este é mais um teste de texto simples. com bootstrap e material com samll text', 'text-danger')
                            ], 'col-6', 'mt-1'),
                            div([
                                img('https://images.pexels.com/photos/255379/pexels-photo-255379.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', 'img-size-100', '', '', ''),
                                div([
                                    button('btnlink', 'button', '', 'add', 'btn btn-success btn-sm text-light mt-1'),
                                    button('btnlink2', 'button', '', 'remove', 'btn btn-info btn-sm text-light mt-1'),
                                    button('btnlink3', 'button', '', 'share', 'btn btn-warning btn-sm text-light mt-1'),
                                    button('btnlink3', 'button', '', 'refresh', 'btn btn-secondary btn-sm text-light mt-1'),
                                ], 'btn-group')
                            ], 'col-6 mt-1'),
                            div([
                                img('https://images.pexels.com/photos/255379/pexels-photo-255379.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', 'img-size-100', '', '', '')
                            ], 'col-6 mt-1')
                        ], 'row text-center')
                    ], 'container')
                ], 'photos', 'text-center'),

                tab_panel([
                    text('h3', 'Carros', 'mdc-typography--headline6 text-center'),
                ], 'carros'),
                tab_panel([
                    text('h3', 'Aluguel', 'mdc-typography--headline6 text-center'),
                ], 'alugue'),
                tab_panel([
                    text('h3', 'Android', 'mdc-typography--headline6 text-center'),
                ], 'android'),
                tab_panel([
                    text('h3', 'Novas teste', 'mdc-typography--headline6 text-center'),
                    div([
                        text('h3', 'Div class aqui', 'mdc-typography--headline6 text-center'),
                    ])
                ], 'file'),
                tab_panel([
                    // text('h3', 'Gifes', 'mdc-typography--headline6 text-center'),
                    div([
                        text_field('text', 'Buscar gifs', 'gifs_text', 'search', 'mdc-text-field--filled', [],'onkeyup="searchGifs_d(this.value)"'),
                        div([], 'gifs-results row'),
                    ], 'container mt-1'),
                    div([], 'loadding_row'),
                ], 'gifs')
            ], 'slider', 'slider'),
        
        ], '', 'fl'),
        div('', '', 'fab'),
        /*
        grid_cell([
            card([
                card_primary_action([
                    card_media('https://images.pexels.com/photos/255379/pexels-photo-255379.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', '')
                ]),
                card_secondary([
                    text('h2', 'Titulo 2 para teste', 'mdc-typography mdc-typography--headline6'),
                    text('h3', 'Titulo 2 para teste', 'mdc-typography mdc-typography--subtitle2')
                ]),
                card_actions([
                    text('i', 'share', 'material-icons'),
                    text('i', 'link', 'material-icons'),
                    text('i', 'check', 'material-icons'),
                ]),

            ]),
        ])*/

    ], '', 'flact');

    return inner;
}

render('app', content());

// function init_app (app) {


// }
// init_app('app');

/*init_app = {
    init: function(app) {
        let inner = "";

        inner += create_components.new_component_div([
        ], 'bg-opacite');
        inner += create_components.new_component_div([
        ], 'bg-splash-2 ');
        inner += create_components.new_component_div([
            // create_components.loading(),
            create_components.new_component_div([
                create_components.new_component_text('h2', [
                    create_components.new_component_text('h1', 'Login', 'text-center text-primary mt-2 p-3 font-weight-bolder'),
                ], 'text-primary text-center'),
                create_components.new_component_form('form-login', [
                    create_components.new_component_div([
                        create_components.new_component_div([
                            create_components.new_component_btn('', 'button',
                                [
                                    create_components.new_component_text('span', 'email', 'material-icons-outlined')
                                ], 'btn btn-transparent pt-3 text-primary'),
                            create_components.new_component_input('text', 'form-control  mt-2 input-border-bottom', 'Email', 'email'),

                        ], 'btn-group'),

                        create_components.new_component_div([
                            create_components.new_component_btn('', 'button',
                                [
                                    create_components.new_component_text('span', 'lock', 'material-icons-outlined')
                                ], 'btn btn-transparent pt-3 text-primary'),
                            create_components.new_component_input('password', 'form-control  mt-2 input-border-bottom', 'Password', 'password'),

                        ], 'btn-group'),
                        create_components.new_component_text('h5', [
                            create_components.new_component_btn('btn-id', 'submit', "Entrar no app", 'btn btn-sm btn-primary mt-3')
                        ], 'text-right'),
                        create_components.new_component_link('Esqueceu a senha?', 'javascript:;', ''),
                        '<br>',
                        create_components.new_component_link('Criar conta', '?route=create_account', ''),

                    ], 'form-group p-2'),

                ], 'form bg-white shadow pb-2 p-1')
            ], 'mt-2 p-2')
        ], 'container splash-front');

        document.getElementById(app).innerHTML += inner;

    }

}

init_app.init("app");
*/
