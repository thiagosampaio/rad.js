import {
    new_component_card as card,
    new_component_card_primary_action as card_primary_action,
    new_component_card_primary as card_primary,
    new_component_card_media as card_media,
    new_component_grid as grid,
    new_component_div as div,
    new_component_main as main,
    new_component_text as text,
    new_component_card_secundary as card_secondary,
    new_component_card_actions as card_actions,
    new_component_linear_progress as progress_bar,
    new_component_float_actions as float_actions, new_component_grid_cell as grid_cell,
    new_component_tab_panel as tab_panel,
    new_component_img as img,
    new_component_link as link,
    new_component_list as list,
    new_component_list_item as list_item,
    new_component_list_item_two_lines as list_item_two_line,
    new_component_list_item_two_lines_icon as list_item_two_line_icon,
    new_component_list_divider as list_divider,
    new_component_btn as button,
    new_component_text_field as text_field,
    new_component_paginate as paginate,
    loading,
    fetchGet,
    render,
    new_timeStamp,
    new_md5,
    loadingRemove
} from './../../../components.js'

import {getUrlParametros, routes, set_routes } from "./../../../../modules/init_pages/init_pages.js";

const url_def = 'https://gateway.marvel.com/v1/public/characters';



export function content() {
    let inner = "";
    inner += list([
    			/*text('h4', 'lista simples'),
    			list_item(['lista 01', 'lista 02', 'liste 03'], 'p-2'),
    			text('h4', 'lista duas linhas'),

    			list_item_two_line(['Teste de link'], ['esta é a segunda linha'], 'p-2'),
    			list_item_two_line(['icone 2'], ['link 02'], 'p-2'),
    			list_item_two_line(['Compartilhar'], ['Compartilar este link'], 'p-2'),
    			text('h4', 'lista duas linhas com icone'),

    			list_item_two_line_icon(['Teste de link'], ['esta é a segunda linha'], ['home'], 'p-2'),
    			list_item_two_line_icon(['icone 2'], ['link 02'], ['account_circle'], 'p-2'),
    			list_item_two_line_icon(['Compartilhar'], ['Compartilar este link', list_divider()], ['share'], 'p-2'),*/

    			
    	], 'mdc-list mdc-list--two-line p-2')

    return inner;
}

export function getAllCaracters () {
    fetch(url_def + '?limit=100&ts='+new_timeStamp()+'&apikey=e91bdb517e802244d8e3ce41525ba515&hash='+new_md5(new_timeStamp() + "10e72ba2e4df72fb4323c40ed7ca72ddb509f41c"+"e91bdb517e802244d8e3ce41525ba515"))
    .then(function(response) {
        return response.json();
    })
    .then(function(jsonResponse) {
        console.log(jsonResponse);
        let content = ""; 
        let json = jsonResponse.data.results;
        let content_container = div([], 'container', 'content_container');
        let content_pai = div([], 'row', 'view_pai_row');
        let pg = paginate(jsonResponse.data.total, 1, jsonResponse.data.limit);

        // console.log([pg]);

        for(let i=0; i < json.length; i++) {
            content += div([
                div([
                    /*div([
                        text('h4', json[i].name, 'font-marvel-ligth text-center text-white p-2', 'style="background-image: url('+json[i].thumbnail.path +'.'+ json[i].thumbnail.extension+');"'),
                    ], 'card-header dark-header p-0'),*/
                    div([
                        // text('h4', json[i].name, 'font-marvel-ligth text-center p-2'),
                        div([
                            text('h4', json[i].name, 'font-marvel-ligth text-center mt-5 text-white p-2'),
                        ], 'bg-dark-opacite align-items-center'),
                        img(json[i].thumbnail.path +'.'+ json[i].thumbnail.extension, 'img-thumbnail', '', '', '', 'style="width:100%;"'),
                    ], 'card-body'),
                    div([
                        button('btn-share-'+i, 'button', '', 'info', 'mdc-button', 'onclick="'+set_routes('view_caracters&id_caracter='+json[i].id)+';"'),
                        button('btn-share-'+i, 'button', '', 'share', 'mdc-button'),
                        button('btn-share-'+i, 'button', '', 'shopping_cart', 'mdc-button'),
                        button('btn-share-'+i, 'button', '', 'favorite_border', 'mdc-button')
                    ], 'card-footer dark-header text-center')
                ], 'card shadow-sm')
                
            ], 'col-12 col-lg-4 mb-2');
        }
        
        // render('fab', pg);
        render('listas', content_container);
        render('content_container', content_pai);
        render('view_pai_row', content);
        loadingRemove();

    });
}

getAllCaracters();


// document.getElementById('carros').innerHTML += content();
