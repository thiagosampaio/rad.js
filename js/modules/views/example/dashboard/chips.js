import {
    new_component_card as card,
    new_component_card_primary_action as card_primary_action,
    new_component_card_primary as card_primary,
    new_component_card_media as card_media,
    new_component_grid as grid,
    new_component_div as div,
    new_component_main as main,
    new_component_text as text,
    new_component_card_secundary as card_secondary,
    new_component_card_actions as card_actions,
    new_component_linear_progress as progress_bar,
    new_component_float_actions as float_actions, new_component_grid_cell as grid_cell,
    new_component_tab_panel as tab_panel,
    new_component_img as img,
    new_component_link as link,
    new_component_btn as button,
    new_component_text_field as text_field,
    loading,
    fetchGet,
    render,
} from './../../../components.js'

export function content() {
    let inner = "";
    inner += text('h3', 'Chips.js', 'text-center');

    // alertOk();

    return inner;
}
render('chips', content());

function alertOk() {
	alert("OK");
}