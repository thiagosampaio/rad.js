init_app = {
    init: function(app) {
        let inner = "";

        inner += create_components.new_component_div([
        ], 'bg-opacite');
        inner += create_components.new_component_div([
        ], 'bg-splash-3 ');
        inner += create_components.new_component_div([
            // create_components.loading(),
            create_components.new_component_div([
                create_components.new_component_text('h2', [
                    create_components.new_component_text('h1', 'ACCOUNT', 'text-center text-primary mt-2 p-3 font-weight-bolder'),
                ], 'text-primary text-center'),
                create_components.new_component_form('form-login', [
                    create_components.new_component_div([
                        create_components.new_component_div([
                            create_components.new_component_btn('', 'button',
                                [
                                    create_components.new_component_text('span', 'account_circle', 'material-icons-outlined')
                                ], 'btn btn-transparent pt-3 text-primary'),
                            create_components.new_component_input('text', 'form-control  mt-2 input-border-bottom', 'Nome de usuário', 'name'),

                        ], 'btn-group p-0'),

                        create_components.new_component_div([
                            create_components.new_component_btn('', 'button',
                                [
                                    create_components.new_component_text('span', 'email', 'material-icons-outlined')
                                ], 'btn btn-transparent pt-3 text-primary'),
                            create_components.new_component_input('text', 'form-control  mt-2 input-border-bottom', 'Email', 'email'),

                        ], 'btn-group p-0'),

                        create_components.new_component_div([
                            create_components.new_component_btn('', 'button',
                                [
                                    create_components.new_component_text('span', 'lock', 'material-icons-outlined')
                                ], 'btn btn-transparent pt-3 text-primary'),
                            create_components.new_component_input('password', 'form-control  mt-2 input-border-bottom', 'Password', 'password'),

                        ], 'btn-group p-0'),
                        create_components.new_component_text('h5', [
                            create_components.new_component_btn('btn-id', 'submit', "Criar conta", 'btn btn-sm btn-primary mt-3')
                        ], 'text-right'),
                        create_components.new_component_link('Fazer login', '?route=login', ''),

                    ], 'form-group p-0'),

                ], 'form bg-white shadow pb-2 p-3')
            ], 'mt-2 p-2')
        ], 'container splash-front');

        document.getElementById(app).innerHTML += inner;
    }
}

init_app.init('app');
