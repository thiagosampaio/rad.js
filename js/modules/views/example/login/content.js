/*card([
    card_primary_action([
        card_media('https://images.pexels.com/photos/255379/pexels-photo-255379.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', '')
    ]),
    card_secondary([
        text('h2', 'Titulo 2 para teste', 'mdc-typography mdc-typography--headline6'),
        text('h3', 'Titulo 2 para teste', 'mdc-typography mdc-typography--subtitle2')
    ]),
    card_actions([
        text('i', 'share', 'material-icons'),
        text('i', 'link', 'material-icons'),
        text('i', 'check', 'material-icons'),
    ]),

]),
card([
    card_primary_action([
        card_media('https://images.pexels.com/photos/255379/pexels-photo-255379.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500', '')
    ]),
    card_secondary([
        text('h2', 'Titulo 2 para teste', 'mdc-typography mdc-typography--headline6'),
        text('h3', 'Titulo 2 para teste', 'mdc-typography mdc-typography--subtitle2')
    ]),
    card_actions([
        text('i', 'share', 'material-icons'),
        text('i', 'link', 'material-icons'),
        text('i', 'check', 'material-icons'),
    ]),

])*/


import {
    new_component_card as card,
    new_component_card_primary_action as card_primary_action,
    new_component_card_primary as card_primary,
    new_component_card_media as card_media,
    new_component_grid as grid,
    new_component_div as div,
    new_component_main as main,
    new_component_text as text,
    new_component_card_secundary as card_secondary,
    new_component_card_actions as card_actions,
    new_component_linear_progress as progress_bar,
    new_component_text_field as text_field,
    new_component_img as img,
    new_component_btn,
    new_component_button,
    new_component_linear_progress as linear_progress,
    new_component_grid_cell as grid_cell, getRouteOnclick,
    new_component_link as links
} from './../../../components.js'

export function content() {

    let inner = "";
    /**
     * Progress Bar
     * @type {string}
     */
    inner += linear_progress();
    /**
     * @type {string}
     */
     // mdc-typography--headline3
    inner += div([
        // img('./img/200.gif', 'img-fluid mt-5 img-splash'),
        text('span', 'account_circle', 'text-center mt-5 text-light material-icons material-icons-xlg'),
        text('h1', 'LOGIN', 'text-center font-marvel text-light mt-3 '),
        div([], 'bg-red'),
        div([
            text_field('email', 'Email', 'email', 'email', 'mdc-text-field--filled'),
            text_field('password', 'Senha', 'senha', 'password', 'mdc-text-field--filled'),
            div([
                
                text('p', links('Esqueceu a senha?', 'javascript:;', 'text-warning'), 'text-left'),
                new_component_btn('idteste', 'button', 'Entrar', 'login','mdc-button mdc-button--raised', 'onclick='+getRouteOnclick('dashboard')+''),
                new_component_btn('idteste_2', 'button', 'Cadastrar', '', 'mdc-button mdc-button--raised ml-2', 'onclick='+getRouteOnclick('cadastrar')+''),
            ], 'align-center text-center mt-2')
        ],'p-3, col-lg-6 col-xs-12 mt-5 ml-auto mr-auto')

    ], 'text-center align-center splash-front  bg-02');
    

    return inner;
}

document.getElementById('app').innerHTML += content();

