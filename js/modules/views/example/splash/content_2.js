init_app = {
    init: function (app) {
        let app_text = "";/*
        app_text += create_components.new_component_div([
        ], 'bg-opacite');
        app_text += create_components.new_component_div([
        ], 'bg-splash-2 ');*/
        app_text += create_components.new_component_div([
            create_components.new_component_div([
                create_components.new_component_img('./img/ng_calling.svg', 'img-fluid'),
                create_components.new_component_div([
                    create_components.new_component_text('h1', [
                            "Chamadas Telefônicas"
                        ],
                        "text-center text-primary mt-2 font-weight-bolder")
                ], 'col-12'),
                create_components.new_component_div([
                    create_components.new_component_text('h1', [
                        create_components.new_component_button('button',
                            [
                                create_components.new_component_text('span', 'login', 'material-icons-outlined material-icons-xlg')
                            ],
                            'btn btn-default text-primary', '', 'onclick="routes.open_route(\'login\')"')
                    ], "text-center mt-5")
                ], 'col-12')
            ], 'row')
        ], 'container splash-front');
        document.getElementById(app).innerHTML += app_text;
    }
}

init_app.init('app');
