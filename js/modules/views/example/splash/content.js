import {
    new_component_card as card,
    new_component_card_primary_action as card_primary_action,
    new_component_card_primary as card_primary,
    new_component_card_media as card_media,
    new_component_grid as grid,
    new_component_div as div,
    new_component_main as main,
    new_component_text as text,
    new_component_card_secundary as card_secondary,
    new_component_card_actions as card_actions,
    new_component_linear_progress as progress_bar,
    new_component_text_field as text_field,
    new_component_img as img,
    new_component_btn,
    new_component_button,
    new_component_linear_progress as linear_progress,
    new_component_grid_cell as grid_cell, getRouteOnclick,
    new_component_float_actions as float_actions,
    new_timeStamp,
    new_md5
} from './../../../components.js'
export function content() {
    console.log(new_timeStamp(), new_md5(new_timeStamp() + "10e72ba2e4df72fb4323c40ed7ca72ddb509f41c"+"e91bdb517e802244d8e3ce41525ba515"));
    let inner = "";
    /**
     * Progress Bar
     * @type {string}
     */
    inner += linear_progress();

    inner += div([
        div([], 'bg-red'),
        div([]),
        div([
            div([
                text('h1', 'MARVEL', ' text-center text-white font-weight-bolder font-marvel'),
                text('h3', 'CARDS COLECIONAVEIS', ' text-center text-white font-weight-bolder font-marvel-ligth'),
                text('p', 'Tenha colecionaveis da MARVEL, crie já uma conta.', ' text-center text-white font-marvel-ligth'),
                // new_component_btn('idteste', 'button', 'Next', 'arrow_forward','mdc-button--raised mdc-button', 'onclick='+getRouteOnclick('login')+''),
                float_actions('arrow_forward', 'NEXT', 'onclick="'+getRouteOnclick('login')+'"'),
            ], 'col'),

        ], 'text-center mt-5 pt-5'),
        

    ], 'splash-front text-center bg-01 pt-5');

    return inner;
}
/*init_app = {
    init: function (app) {
        let app_text = "";

        app_text += create_components.new_component_div([
            create_components.new_component_div([
                create_components.new_component_img('./img/ng_msg_send.svg', 'img-fluid'),

                create_components.new_component_div([
                    create_components.new_component_text('h1', [
                            "Mensagens Instantâneas grátis"
                        ],
                        "text-center text-primary mt-2 p-3 font-weight-bolder")
                ], 'col-12'),
                create_components.new_component_div([
                    create_components.new_component_text('h1', [
                        create_components.new_component_button('button',
                            [
                                // create_components.new_component_text('span', 'arrow_right', 'material-icons-outlined material-icons-xlg')
                            ],
                            'btn btn-default text-primary', '', 'onclick="routes.open_route(\'login\')"')
                    ], "text-center mt-5")
                ], 'col-12')
            ], 'row')
        ], 'container splash-front');

        document.getElementById(app).innerHTML += app_text;
    }
}

init_app.init('app');*/
document.getElementById('app').innerHTML += content();
