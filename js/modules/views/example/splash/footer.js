create_components.new_component_footer([

    create_components.new_component_button('button', [
        create_components.new_component_text('span', [
            'arrow_right'
        ], 'material-icons-outlined material-icons-lg')
    ], 'btn btn-default text-primary btn-block col', '', 'onclick="routes.open_route(\'splash_2\')"'),
], 'fixed-bottom row');
