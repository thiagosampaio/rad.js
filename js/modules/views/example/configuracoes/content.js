import {
    new_component_card as card,
    new_component_card_primary_action as card_primary_action,
    new_component_card_primary as card_primary,
    new_component_card_media as card_media,
    new_component_grid as grid,
    new_component_div as div,
    new_component_main as main,
    new_component_text as text,
    new_component_card_secundary as card_secondary,
    new_component_card_actions as card_actions,
    new_component_linear_progress as progress_bar,
    new_component_float_actions as float_actions, new_component_grid_cell as grid_cell,
    new_component_tab_panel as tab_panel,
    new_component_img as img,
    new_component_link as link,
    new_component_btn as button,
    new_component_text_field as text_field,
    new_component_form as form,
} from './../../components.js'

export function content() {
    let inner = "";

    inner += div([
        progress_bar(),
        // float_actions('call', 'Ligar'),
        form('', [
            div([
                text('h4', 'Dados pessoais', 'text-left'),
                text_field('text', 'Nome Completo', 'nome', 'account_circle', 'mdc-text-field--filled'),
                text_field('text', 'Email', 'email', 'email', 'mdc-text-field--filled'),
                text_field('password', 'Senha', 'senha', 'password', 'mdc-text-field--filled'),
                text('h4', 'Endereço', 'text-left'),
                text_field('text', 'Rua', 'rua', 'home', 'mdc-text-field--filled'),
                text_field('text', 'Bairro', 'bairro', 'maps_home_work', 'mdc-text-field--filled'),
                text_field('text', 'Número', 'numero', 'maps_home_work', 'mdc-text-field--filled'),
                div([
                  button('idsdfg', 'submit', 'Salvar', 'save', 'mdc-button mdc-button--raised mdc-button--leading ml-1'),
                  button('idsdfgas', 'button', 'Limpar', 'clear', 'mdc-button mdc-button--raised mdc-button--leading ml-1'),
                ], 'text-right')
                
            ], 'container mt-5 pt-2'),
        ], 'form mb-4', '', '', 'POST')
    ]);

    return inner;
}

document.getElementById('app').innerHTML += content();