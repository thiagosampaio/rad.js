import {
    new_component_card as card,
    new_component_card_primary_action as card_primary_action,
    new_component_card_primary as card_primary,
    new_component_card_media as card_media,
    new_component_grid as grid,
    new_component_div as div,
    new_component_main as main,
    new_component_text as text,
    new_component_card_secundary as card_secondary,
    new_component_card_actions as card_actions,
    new_component_linear_progress as progress_bar,
    new_component_float_actions as float_actions, new_component_grid_cell as grid_cell,
    new_component_tab_panel as tab_panel,
    new_component_img as img,
    new_component_link as link,
    new_component_btn as button,
    new_component_for as for_increment,
    new_component_text_field as text_field,
    loading,
    loadingRemove,
    render,
    get_params_url,
    new_timeStamp,
    new_md5,
} from './../../../components.js'

export function content() {
	let inner = "";

	inner += div([
		progress_bar(),
		div([
			div([
				div([
					// text('h3', 'view caracter', 'text-center')
				], 'col', 'caracter')
			], 'row')
		], 'container ')
	], 'mdc-top-app-bar--fixed-adjust bg-content');
    inner += loading();


	return inner;
}



function getAllComicsID(url, i) {
    fetch(url+'?ts='+new_timeStamp()+'&apikey=e91bdb517e802244d8e3ce41525ba515&hash='+new_md5(new_timeStamp() + "10e72ba2e4df72fb4323c40ed7ca72ddb509f41c"+"e91bdb517e802244d8e3ce41525ba515"))
        .then(function(response) {
            return response.json();
        })
        .then(function(jsonResponse) {
            let json_comics = jsonResponse.data.results;
            console.log(json_comics);
            let content = "";
            content += div([
                    text('h3', json_comics[0].title)
                ], 'backdrop-close', 'backdrop_comics_' + i, 'style="width:100%;" onclick=\'actOnDrop("backdrop_comics_' + i+'\")\'')
            content += img(json_comics[0].thumbnail.path +'.'+ json_comics[0].thumbnail.extension, 'img-thumbnail img-hover', '', '', '', 'style="width:100%;" onclick=\'actOnDrop("backdrop_comics_' + i+'\")\'');

            render('comics_' + i, content);
        });
}

function getAllSeriesID(url) {
    fetch(url+'?ts='+new_timeStamp()+'&apikey=e91bdb517e802244d8e3ce41525ba515&hash='+new_md5(new_timeStamp() + "10e72ba2e4df72fb4323c40ed7ca72ddb509f41c"+"e91bdb517e802244d8e3ce41525ba515"))
        .then(function(response) {
            return response.json();
        })
        .then(function(jsonResponse) {

            let json_comics = jsonResponse.data.results;
            // console.log(json_comics.length, json_comics);
            let content = "";
            for(let i = 0; i<json_comics.length; i++) {
                content += div([
                    text('h6', json_comics[i].title, 'font-marvel text-danger mt-3 mb-3 text-uppercase'),
                    img(json_comics[i].thumbnail.path +'.'+ json_comics[0].thumbnail.extension, 'img-thumbnail img-hover', '', '', '', 'style="width:100%;"')
                    ], 'col-12 col-lg-4 col-md-4 float-left');
            }

            render('series', content);  
        });
}


function getAllStoriesID(url) {
    // console.log(url);
    for(let i=0; i<url.length; i++) {
        fetch(url[i].resourceURI+'?ts='+new_timeStamp()+'&apikey=e91bdb517e802244d8e3ce41525ba515&hash='+new_md5(new_timeStamp() + "10e72ba2e4df72fb4323c40ed7ca72ddb509f41c"+"e91bdb517e802244d8e3ce41525ba515"))
        .then(function(response) {
            return response.json();
        })
        .then(function(jsonResponse) {
            let json_comics = jsonResponse.data.results;
            // console.log(json_comics.length, json_comics);
            console.log('Json comics', json_comics)
            let content = "";
            let img_df = json_comics[0].thumbnail != null ? img(json_comics[0].thumbnail.path +'.'+ json_comics[0].thumbnail.extension, 'img-thumbnail img-hover', '', '', '', 'style="width:100%;"') : ""
            
            content += div([
                text('h6', json_comics[0].title, 'font-marvel text-light'),
                img_df
                // img(json_comics[0].thumbnail.path +'.'+ json_comics[0].thumbnail.extension, 'img-thumbnail', '', '', '', 'style="width:100%;"')
                ], 'col-12 col-lg-4 col-md-4 float-left');

            render('stories', content);  
        });

    }
    
}
function getAllEventosID(url) {
    fetch(url+'?ts='+new_timeStamp()+'&apikey=e91bdb517e802244d8e3ce41525ba515&hash='+new_md5(new_timeStamp() + "10e72ba2e4df72fb4323c40ed7ca72ddb509f41c"+"e91bdb517e802244d8e3ce41525ba515"))
        .then(function(response) {
            return response.json();
        })
        .then(function(jsonResponse) {
            let json_events = jsonResponse.data.results;
            console.log([json_events.length, json_events]);
            let content = "";
            for(let i = 0; i<json_events.length; i++) {
                content += div([
                    text('h6', json_events[i].title, 'font-marvel text-light'),
                    img(json_events[i].thumbnail.path +'.'+ json_events[0].thumbnail.extension, 'img-thumbnail img-hover', '', '', '', 'style="width:100%;"')
                    ], 'col-12 col-lg-4 col-md-4 float-left');
            }

            render('events', content);  
        });
}


function getAllCaractersID() {
	let id_caracter = get_params_url('id_caracter');
	const url_def = 'https://gateway.marvel.com/v1/public/';

    // json[0].comics.items[i].resourceURI

	
    fetch(url_def + 'characters/'+id_caracter+'?ts='+new_timeStamp()+'&apikey=e91bdb517e802244d8e3ce41525ba515&hash='+new_md5(new_timeStamp() + "10e72ba2e4df72fb4323c40ed7ca72ddb509f41c"+"e91bdb517e802244d8e3ce41525ba515"))
    .then(function(response) {
        return response.json();
    })
    .then(function(jsonResponse) {

        console.log(jsonResponse);
        let content = "";
        let json = jsonResponse.data.results;
        let result_comics =JSON.parse(for_increment(json[0].comics.items,json[0].comics.items.length));
        var comics_content = "";
        var series_content = "";
        var stories_content = "";

        for(let i=0; i<json[0].comics.items.length;i++) {
            getAllComicsID(json[0].comics.items[i].resourceURI, i);

            comics_content += div([
                text('h5', json[0].comics.items[i].name, 'text-left mt-3 mb-3 font-marvel-ligth text-danger col-12 col-lg-12 col-md-12'),
                ], 'col-12 col-lg-4 col-md-4', 'comics_' + i)
            // console.log(json[0].comics.items[i])
        }
        // console.log(json[0].stories.items);
        comics_content += 
        getAllSeriesID(json[0].series.collectionURI);
        comics_content +=  getAllEventosID(json[0].events.collectionURI);

        comics_content +=  getAllStoriesID(json[0].stories.items);
        document.querySelector('.mdc-top-app-bar__title').innerHTML = json[0].name;
        content += div([
        	div([
        		img(json[0].thumbnail.path +'.'+ json[0].thumbnail.extension, 'img-thumbnail', '', '', '', 'style="width:100%;"')
        	], 'col-12 col-lg-4 p-2'),
        	div([
        		text('h2', json[0].name, 'font-marvel text-light mt-5  col-12 col-lg-12 col-md-12 mr-auto ml-auto'),
        		text('h2', "ABOUT", 'text-left inlineModuleHeader__header mb-3 font-marvel text-light  col-12 col-lg-12 col-md-12 mr-auto ml-auto'),
        		text('h5', json[0].description, 'text-left font-marvel-ligth text-light  col-12 col-lg-12 col-md-12 mr-auto ml-auto'),
                

        	], 'col-12 col-lg-8 p-3 text-center'),
            div([
                text('h2', "COMICS:", 'text-left inlineModuleHeader__header font-marvel text-light col-12 col-lg-12 col-md-12 mr-auto ml-auto'),
                div([
                    comics_content
                ], 'row'),
            ], 'col-12 col-lg-12 p-3 text-center'),
            div([
                text('h2', "EVENTS:", 'text-left inlineModuleHeader__header font-marvel text-light  col-12 col-lg-12 col-md-12 mr-auto ml-auto'),
                
            ], 'col-12 col-lg-12 p-3 text-center', 'events'),
            div([
                text('h2', "SERIES:", 'text-left inlineModuleHeader__header font-marvel text-light  col-12 col-lg-12 col-md-12 mr-auto ml-auto'),
                
            ], 'col-12 col-lg-12 p-3 text-center', 'series'),
            div([
                text('h2', "STORIES:", 'text-left inlineModuleHeader__header font-marvel text-light  col-12 col-lg-12 col-md-12 mr-auto ml-auto'),
                
            ], 'col-12 col-lg-12 p-3 text-center', 'stories')
        ], 'row', '', 'style="background-image:url('+json[0].thumbnail.path +''+ json[0].thumbnail.extension+'); background-repeat: no-repeat; background-size:cover; background-position:center; background-position:fixe; min-height:100%;"')
        

    	render('caracter', content);
        loadingRemove();
    })
}

getAllCaractersID()

render('app', content())
function actOnDrop(id) {
    var element = document.getElementById(id);
    element.classList.remove("backdrop-close");
}