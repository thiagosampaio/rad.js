import {
    new_component_footer,
    new_component_btn as btn,
    new_component_btn_top as btn_top,
    render
} from './../../../components.js'


export function content() {
    let inner = new_component_footer([
        // btn('button', [
        //     new_component_text('span', [
        //         'arrow_left'
        //     ], 'material-icons-outlined material-icons-lg')
        // ], 'btn btn-default text-primary btn-block col-6', '', 'onclick="routes.open_route(\'splash\')"'),
        // new_component_button('button', [
        //     new_component_text('span', [
        //         'arrow_right'
        //     ], 'material-icons-outlined material-icons-lg')
        // ], 'btn btn-default text-primary btn-block col-6', '', 'onclick="routes.open_route(\'splash_2\')"'),
        btn('', 'button', '', 'home', 'mdc-button flex-fill p-4 text-danger'),
        btn_top('', 'button', '', 'manage_accounts', 'mdc-button flex-fill p-4 text-danger btn-float-menu--footer'),
        btn('', 'button', '', 'menu', 'mdc-button flex-fill p-4 text-danger'),
        btn('', 'button', '', 'edit_attributes', 'mdc-button flex-fill p-4 text-danger'),
        btn_top('', 'button', '', 'logout', 'mdc-button flex-fill p-4 text-danger'),
    
    
    ], 'fixed-bottom p-1 bg-white d-flex bd-highlight');

    return inner;
}
render('app', content());


