import {
    new_component_menu as menu,
    new_component_menu_header as menu_header,
    new_component_text as text,
    new_component_link as link,
    new_component_separator as separator,
    loading
} from './../../../components.js'

menu([

    link([
        // text('i', 'close', 'material-icons-outlined mdc-list-item__graphic', ''),
        text('span', [], 'mdc-list-item__text', ''),
    ], 'javascript:;', 'mdc-list-item ', 'onclick="closeModal()"'),

    menu_header([
        text('h3', 'Email', 'mdc-drawer__title'),
        text('h4', 'jthiagosampaio@gmail.com', 'mdc-drawer__subtitle')
    ]),
    separator(),
    link([
            text('i', 'dashboard', 'material-icons-outlined mdc-list-item__graphic p-2', ''),
            text('span', ['Dashboard'], 'mdc-list-item__text p-2', ''),
        ], '?route=dashboard', 'mdc-list-item mdc-list-item--activated font-weight-bold', ''),
    link([
            text('i', 'manage_accounts', 'material-icons-outlined mdc-list-item__graphic p-2', ''),
            text('span', ['Configurações'], 'mdc-list-item__text p-2', ''),
        ], '?route=configuracoes', 'mdc-list-item  font-weight-bold', ''),
    link([
            text('i', 'manage_accounts', 'material-icons-outlined mdc-list-item__graphic p-2', ''),
            text('span', ['Gestão de usuários'], 'mdc-list-item__text p-2', ''),
        ], 'javascript:;', 'mdc-list-item   font-weight-bold', ''),
    link([
            text('i', 'logout', 'material-icons-outlined mdc-list-item__graphic p-2', ''),
            text('span', ['Logout'], 'mdc-list-item__text p-2', ''),
        ], 'javascript:;', 'mdc-list-item font-weight-bold', ''),

    link([
            text('i', 'bookmark', 'material-icons-outlined mdc-list-item__graphic p-2', ''),
            text('span', ['Páginas'], 'mdc-list-item__text p-2', ''),
        ], 'javascript:;', 'mdc-list-item font-weight-bold', ''),
    separator()
], 'mdc-drawer--modal p-2', 'account_circle');
