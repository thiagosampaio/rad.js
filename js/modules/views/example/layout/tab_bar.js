import {
    new_component_main as main,
    new_component_tab_bar as tab_bar,
    new_component_linear_progress as linear_progress,
    new_component_tab_panel as tab_panel,
    new_component_text as text,
    new_component_grid as grid,
    new_component_div as div,
    new_component_grid_cell as grid_cell, new_component_float_actions as float_actions,
    loading,
    loadingRemove
} from './../../../components.js'

export function content_app_bar() {
    let inner = "";
    inner += main([
        loading(),
        linear_progress(),
        
        tab_bar(
            ['Caracters', 'Chips', 'Casas', 'Carros', 'Aluguel', 'Android', 'News',  'Gifs'],
            ['list','edit_attributes','chair', 'directions_car', 'apartment', 'android', 'favorite_border', 'gif_box'],
        ),
    ]);
    return inner;
}

document.getElementById('app').innerHTML += content_app_bar();
