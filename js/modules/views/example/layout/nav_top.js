import {
    new_component_btn_app_bar,
    new_component_header as header,
    new_component_linear_progress as progress_bar
} from './../../../components.js'
import { new_component_btn as button } from '../../../components.js'
import {getUrlParametros} from "./../../../init_pages/init_pages.js";


header([
    new_component_btn_app_bar('file_download'),
    new_component_btn_app_bar('print'),
    new_component_btn_app_bar('bookmark'),
    
], 'standar', getUrlParametros());

