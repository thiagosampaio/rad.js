export function header(dt_array = Array()) {
    let dt = "";
        for (let i = 0; i < dt_array.length; i++) {
            dt += dt_array[i];
        }
        document.getElementById('header').innerHTML = dt;
}
export function style_tag (defaults) {

    return "<style>" + defaults + "</style>";
}
export function title (defaults) {

    return "<title>" + defaults + "</title>";
}

export function links (defaults = Array()) {
    let df = "";
    for (let i = 0; i < defaults.length; i++) {
        df += "<link href=\"" + defaults[i] + "\" rel=\"stylesheet\"/>";
    }
    return df;
}

export function metatags (defaults = Array()) {
    let df = "";
    for (let i = 0; i < defaults.length; i++) {
        df += "<meta " + defaults[i] + "/>";
    }
    return df;
}
