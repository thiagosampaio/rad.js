/**
 *
 * Totas as classes e funções estão descritas a baixo
 *
 * @type {{
 * new_component_div: (function(*=, *=): string),
 * new_component_button: (function(*, *, *=, *=, *=): string),
 * new_component_link: (function(*, *, *=): string),
 * new_component_tab: (function(*, *, *=, *=, *=): string),
 * new_component_list: (function(*=, *=, *=): string),
 * new_component_li: (function(*=, *=): string),
 * new_component_btn: (function(*, *, *, *=, *=): string),
 * loading: (function(): string),
 * new_component_header: create_components.new_component_header,
 * new_component_option: (function(*, *): string),
 * new_component_input: (function(*, *=, *=, *=, *, *=): string),
 * new_component_form: (function(*, *=, *=, *=, *=, *=): string),
 * new_component_footer: create_components.new_component_footer,
 * new_component_select: (function(*=, *=, *=, *=): string),
 * new_component_text: (function(*, *=, *=): string),
 * new_component_btn_tabs: (function(*, *=, *=, *=): string),
 * new_component_for: (function(*=, *=): string)}}
 */


/**
 *
 * @param _text
 * @param _href
 * @param _class
 * @returns {string}
 */
export function new_component_separator() {
    return '<hr class="mdc-list-divider">';
}

export function new_timeStamp() {
    var timestamp = new Date().getTime();
    return timestamp;
}
export function new_md5(d) {
    var r = M(V(Y(X(d),8*d.length)));
    return r.toLowerCase()};function M(d){for(var _,m="0123456789ABCDEF",f="",r=0;r<d.length;r++)_=d.charCodeAt(r),f+=m.charAt(_>>>4&15)+m.charAt(15&_);return f}function X(d){for(var _=Array(d.length>>2),m=0;m<_.length;m++)_[m]=0;for(m=0;m<8*d.length;m+=8)_[m>>5]|=(255&d.charCodeAt(m/8))<<m%32;return _}function V(d){for(var _="",m=0;m<32*d.length;m+=8)_+=String.fromCharCode(d[m>>5]>>>m%32&255);return _}function Y(d,_){d[_>>5]|=128<<_%32,d[14+(_+64>>>9<<4)]=_;for(var m=1732584193,f=-271733879,r=-1732584194,i=271733878,n=0;n<d.length;n+=16){var h=m,t=f,g=r,e=i;f=md5_ii(f=md5_ii(f=md5_ii(f=md5_ii(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_hh(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_gg(f=md5_ff(f=md5_ff(f=md5_ff(f=md5_ff(f,r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+0],7,-680876936),f,r,d[n+1],12,-389564586),m,f,d[n+2],17,606105819),i,m,d[n+3],22,-1044525330),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+4],7,-176418897),f,r,d[n+5],12,1200080426),m,f,d[n+6],17,-1473231341),i,m,d[n+7],22,-45705983),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+8],7,1770035416),f,r,d[n+9],12,-1958414417),m,f,d[n+10],17,-42063),i,m,d[n+11],22,-1990404162),r=md5_ff(r,i=md5_ff(i,m=md5_ff(m,f,r,i,d[n+12],7,1804603682),f,r,d[n+13],12,-40341101),m,f,d[n+14],17,-1502002290),i,m,d[n+15],22,1236535329),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+1],5,-165796510),f,r,d[n+6],9,-1069501632),m,f,d[n+11],14,643717713),i,m,d[n+0],20,-373897302),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+5],5,-701558691),f,r,d[n+10],9,38016083),m,f,d[n+15],14,-660478335),i,m,d[n+4],20,-405537848),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+9],5,568446438),f,r,d[n+14],9,-1019803690),m,f,d[n+3],14,-187363961),i,m,d[n+8],20,1163531501),r=md5_gg(r,i=md5_gg(i,m=md5_gg(m,f,r,i,d[n+13],5,-1444681467),f,r,d[n+2],9,-51403784),m,f,d[n+7],14,1735328473),i,m,d[n+12],20,-1926607734),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+5],4,-378558),f,r,d[n+8],11,-2022574463),m,f,d[n+11],16,1839030562),i,m,d[n+14],23,-35309556),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+1],4,-1530992060),f,r,d[n+4],11,1272893353),m,f,d[n+7],16,-155497632),i,m,d[n+10],23,-1094730640),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+13],4,681279174),f,r,d[n+0],11,-358537222),m,f,d[n+3],16,-722521979),i,m,d[n+6],23,76029189),r=md5_hh(r,i=md5_hh(i,m=md5_hh(m,f,r,i,d[n+9],4,-640364487),f,r,d[n+12],11,-421815835),m,f,d[n+15],16,530742520),i,m,d[n+2],23,-995338651),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+0],6,-198630844),f,r,d[n+7],10,1126891415),m,f,d[n+14],15,-1416354905),i,m,d[n+5],21,-57434055),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+12],6,1700485571),f,r,d[n+3],10,-1894986606),m,f,d[n+10],15,-1051523),i,m,d[n+1],21,-2054922799),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+8],6,1873313359),f,r,d[n+15],10,-30611744),m,f,d[n+6],15,-1560198380),i,m,d[n+13],21,1309151649),r=md5_ii(r,i=md5_ii(i,m=md5_ii(m,f,r,i,d[n+4],6,-145523070),f,r,d[n+11],10,-1120210379),m,f,d[n+2],15,718787259),i,m,d[n+9],21,-343485551),m=safe_add(m,h),f=safe_add(f,t),r=safe_add(r,g),i=safe_add(i,e)}return Array(m,f,r,i)}function md5_cmn(d,_,m,f,r,i){return safe_add(bit_rol(safe_add(safe_add(_,d),safe_add(f,i)),r),m)}function md5_ff(d,_,m,f,r,i,n){return md5_cmn(_&m|~_&f,d,_,r,i,n)}function md5_gg(d,_,m,f,r,i,n){return md5_cmn(_&f|m&~f,d,_,r,i,n)}function md5_hh(d,_,m,f,r,i,n){return md5_cmn(_^m^f,d,_,r,i,n)}function md5_ii(d,_,m,f,r,i,n){return md5_cmn(m^(_|~f),d,_,r,i,n)}function safe_add(d,_){var m=(65535&d)+(65535&_);return(d>>16)+(_>>16)+(m>>16)<<16|65535&m}function bit_rol(d,_){return d<<_|d>>>32-_

}

export function render(id, content) {
    try{

        document.getElementById(id).innerHTML += content;
    }catch(e) {
        console.log(e);
        let msg = "";
        msg += "<div class='row'>";
        msg += "<div class='alert-danger col '>";
        msg += "<h6 class='text-center mt-4'>"+e.name+"</h6>";
        msg += "</div>";
        msg += "<div class='bg-secondary col'>";
        msg += "<p class=''>"+e.message+"</p>";
        msg += "</div>";
        msg += "<div class='bg-light col-12'>";
        msg += "<p class=' text-break'>"+e.stack+"</p>";
        msg += "</div>";
        msg += "</div>";
        document.getElementById('app').innerHTML = msg;
    }
}
export function alertError (e) {
    console.log(e);
    let msg = "";
    if(e.name) {
        
        msg += "<div class='row p-2'>";
        msg += "<div class='alert-danger col '>";
        msg += "<h6 class='text-center mt-4'>"+e.name+"</h6>";
        msg += "</div>";
        msg += "<div class='bg-secondary col'>";
        msg += "<p class=''>"+e.message+"</p>";
        msg += "</div>";
        msg += "<div class='bg-light col-12'>";
        msg += "<p class=' text-break'>"+e.stack+"</p>";
        msg += "</div>";
        msg += "</div>";
    }else {

        msg += "<div class='row p-2'>";
        msg += "<div class='alert-danger col '>";
        msg += "<h6 class='text-center mt-4'>Error</h6>";
        msg += "</div>";
        msg += "<div class='bg-secondary col'>";
        msg += "<p class='text-light' style='word-break: break-word;'>"+e+"</p>";
        msg += "</div>";
        msg += "<div class='bg-light col-12'>";
        msg += "<p class=' text-break'></p>";
        msg += "</div>";
        msg += "</div>";
    }
    
    document.getElementById('app').innerHTML = msg;
}
export function new_component_link(_text = Array(), _href, _class = null, _outer = null) {
    let li = "<a href=\"" + _href + "\" class=\"" + _class + " \" " + _outer + "><span class=\"mdc-list-item__ripple\" ></span>";
    for (let i = 0; i < _text.length; i++) {
        li += _text[i];
    }
    li += "</a>";
    console.log(li)
    return li;
};

/**
 *
 * @param _type
 * @param _params
 * @param _class
 * @returns {string}
 */
export function new_component_text(_type, _params = Array(), _class = null, _action = null) {
    let type = "<" + _type + " class=\"" + _class + "\" " + _action + ">";
    for (let i = 0; i < _params.length; i++) {
        type += _params[i];
    }
    type += "</" + _type + ">";
    return type;
};


/**
 * @function new_component_div()
 * @param _params
 * @param _class
 * @returns {string}
 */

export function new_component_text_field(type, _label, _id, _icon, _class = null  ,_event = Array(), _onclick = null) {
    let icon = _icon == null ? "" : '<i class="material-icons mdc-text-field__icon mdc-text-field__icon--leading" tabindex="0" role="button">'+_icon+'</i>'
    let textField = '<label class="mdc-text-field mdc-text-field--with-leading-icon '+_class+'">';
    textField += '<span class="mdc-text-field__ripple"></span>';
    textField += '<span class="mdc-floating-label" id="my-label-id">' + _label + '</span>';
    textField += icon;
    textField += '<input class="mdc-text-field__input" type="' + type + '" id="' + _id + '" aria-labelledby="my-label-id" '+_onclick+'>';
    textField += '<span class="mdc-line-ripple"></span>';
    textField += '</label>';
    
    if(_event.length > 0) {
        let selector = document.querySelector(_event[1]);
        document.addEventListener(_event[0], _event[2]);
        // console.log(selector);
    }
    
    return textField;
}

export function new_component_div(_params = Array(), _class = null, id = null, _action = null) {
    let li = "<div class=\"" + _class + "\" id=\"" + id + "\" "+_action+">";
    for (let i = 0; i < _params.length; i++) {
        li += _params[i];
    }
    li += "</div>";
    return li;
};

export function get_params_url(_params) {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    let url_params = urlParams.get(_params);

    return url_params;
}

export function new_component_paginate(_items, _paginaAtual, _limitItems, url) {
    let totalPages = Math.ceil(_items / _limitItems);
    let count = (_paginaAtual * _limitItems) - _limitItems;
    let delimiter = count + _limitItems;

    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    let url_params = urlParams.get('page');
    let comp = "";
    if(url_params == 1 || url_params == null){
        if(url_params > totalPages){
            comp += new_component_float_actions('arrow_forward', 'next', "onclick=location.href=\'"+window.location+"&page="+(totalPages)+"\';");

        }else {
            comp += new_component_float_actions('arrow_forward', 'next', "onclick=location.href=\'"+window.location+"&page="+(url_params + 1)+"\';");

        }
    }else {
        // render('app', new_component_float_actions('arrow_forward', 'next', ''));
        comp += new_component_float_actions('arrow_back', 'back', "onclick='location.href=\""+window.location+"\"'");
    }

    if(_paginaAtual <= totalPages) {
        for(let i=count; i < delimiter; i++) {
            count++;
        }
    }
    console.log([totalPages, count, delimiter, count, url_params])
    return comp;
}  

/**
 *
 * @param _params
 * @param _name
 * @param _class
 * @param _on_event
 * @returns {string}
 */
export function new_component_select(_params = Array(), _name = null, _class = null, _on_event = null) {
    let li = "<select class=\"" + _class + "\" " + _name + " " + _on_event + ">";
    for (i = 0; i < _params.length; i++) {
        li += _params[i];
    }
    li += "</select>";
    return li;
}

/**
 *
 * @param _params
 * @param _value
 * @returns {string}
 */
export function new_component_option(_params, _value) {
    return "<option " + _params + ">" + _value + "</option>";

}

/**
 *
 * @param _params
 * @param _class
 */
export function new_component_header(_params = Array(), _type = null, _title = null, _class = null) {
    var type_menu = {
        fixed: 'mdc-top-app-bar--fixed',
        prominent: 'mdc-top-app-bar--prominent',
        dense: 'mdc-top-app-bar--dense',
    };
    let li = "<header class=\" mdc-top-app-bar mdc-top-app-bar--" + _type + " " + _class + "\">";
    li += "<div class=\"mdc-top-app-bar__row\">";
        li += "<section class=\"mdc-top-app-bar__section mdc-top-app-bar__section--align-start\" role=\"toolbar\">";
            li += "<button class=\"material-icons mdc-top-app-bar__navigation-icon mdc-icon-button\" id='btn-open-left' onclick='openModal()'>"+
            "<span class='mdc-ripple-surface'>menu</span>";
            li += "</button>";
            li += "<span class=\"mdc-top-app-bar__title\">" + _title.replace('_', ' ') + "</span>";
        li += "</section>";
        li += "<section class=\"mdc-top-app-bar__section mdc-top-app-bar__section--align-end\">"
    for (let i = 0; i < _params.length; i++) {
        li += _params[i];
    }
    // li +=        "<button class=\"mdc-icon-button material-icons mdc-top-app-bar__action-item--unbounded\"  aria-label=\"Download\">file_download
    // li +=        "</button>";
    // li +=        "<button class=\"mdc-icon-button material-icons mdc-top-app-bar__action-item--unbounded\"  aria-label=\"Print this page\">print
    // li +=        "</button>";
    // li +=        "<button class=\"mdc-icon-button material-icons mdc-top-app-bar__action-item--unbounded\"  aria-label=\"Bookmark this page\">bookmark
    // li +=        "</button>";
    li += "</section>";
    li += "</div>";
    li += "</header>";
    li += "<div class=\"mdc-drawer-scrim\"></div>";
    document.querySelector('#app').innerHTML += li;
    // document.querySelector('#app').textContent(li);
}

export function new_component_linear_progress(_class = null) {
    let progress = '<div role="progressbar" class="mdc-linear-progress '+_class+'" aria-label="Example Progress Bar" aria-valuemin="0" aria-valuemax="1" aria-valuenow="0">';
    progress += '<div class="mdc-linear-progress__buffer">';
    progress += '<div class="mdc-linear-progress__buffer-bar"></div>';
    progress += '<div class="mdc-linear-progress__buffer-dots"></div>';
    progress += '</div>';
    progress += '<div class="mdc-linear-progress__bar mdc-linear-progress__primary-bar">';
    progress += '<span class="mdc-linear-progress__bar-inner"></span>';
    progress += '</div>';
    progress += '<div class="mdc-linear-progress__bar mdc-linear-progress__secondary-bar">';
    progress += '<span class="mdc-linear-progress__bar-inner"></span>';
    progress += '</div>';
    progress += '</div>';
    return progress;
}

/**
 *
 * @param _params
 * @param _class
 * @param id
 */
export function new_component_footer(_params = Array(), _class = null, id = null) {
    let li = "<div class=\"" + _class + "\" id='" + id + "'>";
    for (let i = 0; i < _params.length; i++) {
        li += _params[i];
    }
    li += "</div>";
    // document.querySelector('#app').innerHTML += li;
    // document.querySelector('#app').textContent(li);
    let app = document.querySelector('#app');
    let dc = document.querySelectorAll('.content');

    const cont = [].map.call(dc, function(el) {
        // return new mdc.ripple.MDCRipple(el);
        // return new mdc.textField.MDCTextField(el);
        el.style.height = "90vh";
    });

    
    // app.style.height = "100vh";

    return li;
}

/**
 *
 * @param id
 * @param _type
 * @param _params
 * @param _class
 * @param event
 * @returns {string}
 */


export function new_component_btn(id, _type, _params, _icon = null,  _class = null, event = null) {
    let icon = _icon === "" ? "" : "<i class=\"material-icons mdc-button__icon\">"+_icon+"</i>";
    return "<button type=\"" + _type + "\" class=\"" + _class + "\" " + event + " id='" + id + "'>" +
        "<span class=\"mdc-button__ripple\"></span>" +
        icon +
        "<span class=\"mdc-button__label\">" + _params + "</span>" +
        "</button>";
}

export function new_component_btn_top(id, _type, _params, _icon = null,  _class = null, event = null) {
    let icon = _icon === "" ? "" : "<i class=\"material-icons mdc-button__icon\">"+_icon+"</i>";
    return "<button type=\"" + _type + "\" class=\"" + _class + "\" " + event + " id='" + id + "'>" +
        "<span class=\"mdc-button__ripple\"></span>" +
        icon +
        "<br><span class=\"mdc-button__label\">" + _params + "</span>" +
        "</button>";
}

export function new_component_btn_app_bar(icon) {
    let btn = "";
    btn += "<button class=\"mdc-icon-button material-icons mdc-top-app-bar__action-item--unbounded\" aria-label=\""+icon+"\">"
    + "<span class='mdc-ripple-surface'>" +icon +"</span>"
    +"</button>";
    return btn;
}

export function new_component_float_actions(_icon, _text, _action = null) {
    let fl = '<div class="mdc-touch-target-wrapper">'
    fl += '<button class="mdc-fab mdc-fab--extended" '+_action+'>';
    fl += '<div class="mdc-fab__ripple"></div>';
    fl += '<span class="material-icons mdc-fab__icon">'+_icon+'</span>';
    fl += '<span class="mdc-fab__label">'+_text+'</span>';
    // fl += '<span class="material-icons mdc-fab__icon"></span>';
    // fl += '<div class="mdc-fab__touch"></div>';
    fl += '</button>';
    fl += '</div>';
    return fl;
}



/**
 *
 * @param id
 * @param _params
 * @param _class
 * @param event
 * @returns {string}
 */

export function new_component_btn_tabs(id, _params = Array(), _class = null, event = null) {
    let li = "<div class='btn-tabs " + _class + "' id='" + id + "'>";
    for (let i = 0; i < _params.length; i++) {

        li += _params[i];
    }
    li += "</div>";
    return li;
}

export function new_component_grid(_params = Array(), _class = null) {
    let card = "";
    card += '<div class="mdc-layout-grid '+_class+'">';
    card += '<div class="mdc-layout-grid__inner">';
    for (let i = 0; i < _params.length; i++) {
        card += _params[i];
    }
    card += '</div>';
    card += '</div>';
    return card;

}
export function new_component_grid_cell(_params = Array()) {
    let card = "";
    card += '<div class="mdc-layout-grid__cell">';
    for (let i = 0; i < _params.length; i++) {
        card += _params[i];
    }
    card += '</div>';
    return card;

}


export function new_component_card(_params = Array()) {
    let card = "";
    card += '<div class="mdc-card ">';
    for (let i = 0; i < _params.length; i++) {
        card += _params[i];
    }
    card += '</div>';
    return card;
}

export function new_component_card_primary_action(_params = Array()) {
    let card = "";
    card += '<div class="mdc-card__primary-action mdc-card__actions--full-bleed" tabIndex="0">';
    for (let i = 0; i < _params.length; i++) {
        card += _params[i];
    }
    card += '</div>';
    return card;
}

export function new_component_card_media(url, params,) {
    let card = "";
    card += '<div class="mdc-card__media mdc-card__media--16-9" style="background-image: url(' + url + ');">';
    card += '<div class="mdc-card__media-content">' + params + '</div>';
    card += '</div>';

    return card;

}

export function new_component_card_primary(_params = Array()) {
    // console.log(_params)
    let card = "";
    card += '<div class="mdc-card">';
    for (let i = 0; i < _params.length; i++) {
        card += _params[i];
    }
    // card += '<h2 class="demo-card__title mdc-typography mdc-typography--headline6">Our Changing Planet</h2>';
    // card += '<h3 class="demo-card__subtitle mdc-typography mdc-typography--subtitle2">by Kurt Wagner</h3>';
    card += '</div>';
    /*card += '<div class="demo-card__secondary mdc-typography mdc-typography--body2">Visit ten places on our planet hat are undergoing the biggest changes today.';
    card += '</div>';
    card += '</div>';
    card += '<div class="mdc-card__actions">';
    card += '<div class="mdc-card__action-buttons">';
    card += '<button class="mdc-button mdc-card__action mdc-card__action--button"><span class="mdc-button__ripple"></span> Read';
    card += '</button>';
    card += '<button class="mdc-button mdc-card__action mdc-card__action--button"><span class="mdc-button__ripple"></span> Bookmark';
    card += '</button>'
    card += '</div>';
    card += '<div class="mdc-card__action-icons">';
    card += '<button class="mdc-icon-button mdc-card__action mdc-card__action--icon--unbounded" aria-pressed="false" aria-label="Add to favorites" title="Add to favorites">';
    card += '<i class="material-icons mdc-icon-button__icon mdc-icon-button__icon--on">favorite</i>';
    card += '<i class="material-icons mdc-icon-button__icon">favorite_border</i>';
    card += '</button>';


    card += '</div>';*/
    return card;

}

export function new_component_card_secundary(_params = Array()) {
    let card = "";
    card += '<div class="demo-card__secondary mdc-typography mdc-typography--body2">';

    for (let i = 0; i < _params.length; i++) {
        card += _params[i];
    }
    


    card += '</div>';
    return card;

}

export function new_component_card_actions(_params = Array()) {
    // card += '</div>';
    let card = "";
    card += '<div class="mdc-card__actions">';
    card += '<div class="mdc-card__action-buttons">';
    for (let i = 0; i < _params.length; i++) {
        card += '<button class="mdc-button mdc-card__action mdc-card__action--button"><span class="mdc-button__ripple"></span> ' + _params[i];
        card += '</button>';
    }
    card += '</div>';
    card += '</div>';

    // card += '<button class="mdc-button mdc-card__action mdc-card__action--button"><span class="mdc-button__ripple"></span> Read';
    // card += '</button>';
    // card += '<button class="mdc-button mdc-card__action mdc-card__action--button"><span class="mdc-button__ripple"></span> Bookmark';
    // card += '</button>'
    //
    // card += '<div class="mdc-card__action-icons">';
    // card += '<button class="mdc-icon-button mdc-card__action mdc-card__action--icon--unbounded" aria-pressed="false" aria-label="Add to favorites" title="Add to favorites">';
    // card += '<i class="material-icons mdc-icon-button__icon mdc-icon-button__icon--on">favorite</i>';
    // card += '<i class="material-icons mdc-icon-button__icon">favorite_border</i>';
    // card += '</button>';
    return card;
}

export function new_component_tab_panel(_params = Array(), id, _class = null) {
    let r_class = _class == null ? "" : _class;
    let tab = "";
    tab += '<div class="content '+r_class+'" id="'+id+'">';
    for(let i =0; i<_params.length; i++){
        tab += _params[i];
    }

    tab += '</div>';

    return tab;
}
export function new_component_tab_bar(_params = Array(), _icons = Array(), _id_tab = Array(), outer = null,  _class = null) {
    let tab = "";
    tab += '<div class="mdc-tab-bar">';
    tab += '<div class="mdc-tab-scroller">';
    tab += '<div class="mdc-tab-scroller__scroll-area">';
    tab += '<div class="mdc-tab-scroller__scroll-content">';
    for (let i = 0; i < _params.length; i++) {
        // mdc-tab-indicator--active
        // mdc-tab--active

        tab += '<button class="mdc-tab " role="tab" data-target="#'+_id_tab[i]+'" aria-selected="true" '+ outer +'>';
        tab += '<span class="mdc-tab__content">';
        tab += ' <span class="mdc-tab__icon material-icons" aria-hidden="true">' + _icons[i] + '</span><br>';
        tab += '<span class="mdc-tab__text-label">' + _params[i] + '</span>';
        tab += '</span>';
        tab += '<span class="mdc-tab-indicator ">';
        tab += '<span class="mdc-tab-indicator__content mdc-tab-indicator__content--underline"></span>';
        tab += '</span>';
        tab += '<span class="mdc-tab__ripple"></span>';
        tab += '</button>';

    }
    tab += '</div>';
    tab += '</div>';
    tab += '</div>';
    tab += '</div>';

    return tab;
};

/**
 *
 * @param _params
 * @param _class_ul
 * @param _class_li
 * @returns {string}
 */
export function new_component_list(_params = Array(), _class_ul = null, _class_li = null) {
    let li = "<ul class=\"" + _class_ul + "\">";
    for (let i = 0; i < _params.length; i++) {
        li += _params[i];
    }
    li += "</ul>";
    return li;
};

export function new_component_list_item(_params = Array(), _class_li = null) {
    let li = "";
    for (let i = 0; i < _params.length; i++) {
        li += '<li class="mdc-list-item '+_class_li+'" tabindex="'+i+'">';
        li +=     '<span class="mdc-list-item__ripple"></span>';
        li +=     '<span class="mdc-list-item__text">'+_params[i]+'</span>';
        li += '</li>';
    }
    return li;
};

export function new_component_list_item_two_lines(_params = Array(), _params_two = Array(), _class_li = null) {
    let li = "";
    for (let i = 0; i < _params.length; i++) {
        li += '<li class="mdc-list-item '+_class_li+'" tabindex="'+i+'">';
        li +=     '<span class="mdc-list-item__ripple"></span>';
        // li +=     '<span class="mdc-list-item__graphic material-icons '+_class_li+'" aria="true">'+_params_icons[i]+'</span>';
        li +=     '<span class="mdc-list-item__text">';
        li +=       '<span class="mdc-list-item__primary-text">'+_params[i]+'</span>';
        li +=       '<span class="mdc-list-item__secondary-text">'+_params_two[i]+'</span>';
        li +=     '<span>';
        li += '</li>';
    }
    return li;
};

export function new_component_list_item_two_lines_icon(_params = Array(), _params_two = Array(), _params_icons = Array(), _class_li = null) {
    let li = "";
    for (let i = 0; i < _params.length; i++) {
        li += '<li class="mdc-list-item '+_class_li+'" tabindex="'+i+'">';
        li +=     '<span class="mdc-list-item__ripple"></span>';
        li +=     '<span class="mdc-list-item__graphic material-icons '+_class_li+'" aria="true">'+_params_icons[i]+'</span>';
        li +=     '<span class="mdc-list-item__text">';
        li +=       '<span class="mdc-list-item__primary-text">'+_params[i]+'</span>';
        li +=       '<span class="mdc-list-item__secondary-text">'+_params_two[i]+'</span>';
        li +=     '<span>';
        li += '</li>';
    }
    return li;
};
export function new_component_list_divider () {
    return '<li role="separator" class="mdc-list-divider"></li>';
}

/**
 *
 * @param _params
 * @param _class
 * @returns {string}
 */
export function new_component_li(_params = Array(), _class = null) {
    let li = "";
    for (i = 0; i < _params.length; i++) {
        li += "<li class=\"" + _class + "\">" + _params[i] + "</li>";
    }
    return li;
};

/**
 *
 * @param id
 * @param _params
 * @param _class_form
 * @param _class_params
 * @param _action
 * @param _method
 * @returns {string}
 */
export function new_component_form(id, _params = Array(), _class_form = null, _class_params = null, _action =
    null, _method = null) {
    let li = "<form class=\"" + _class_form + "\" action=\"" + _action + "\" method=\"" + _method + "\" id='" + id + "'>";
    for (let i = 0; i < _params.length; i++) {
        li += _params[i];
    }
    li += "</form>";
    return li;
}

/**
 *
 * @param _type
 * @param _class
 * @param _placeholder
 * @param _name
 * @param id
 * @param on_actions
 * @returns {string}
 */
export function new_component_input(_type, _class = null, _placeholder = null, _name = null, id, on_actions =
    null) {
    return "<input id='" + id + "' type=\"" + _type + "\" class=\"" + _class + "\" placeholder=\"" + _placeholder + "\" name=\"" + _name + "\" " + on_actions + ">";
}

export function new_component_img(_src, _class = null, _placeholder = null, _name = null, id, on_actions =
    null) {
    return "<img id='" + id + "' src=\"" + _src + "\" class=\"" + _class + "\" placeholder=\"" + _placeholder + "\" name=\"" + _name + "\" " + on_actions + ">";
}

/**
 *
 * @param _type
 * @param _params
 * @param _class
 * @param _name
 * @param on_actions
 * @returns {string}
 */
export function new_component_button(_type, _params, _class = null, _name = null, on_actions = null) {
    return "<button type=\"" + _type + "\" class=\"" + _class + "\" " + on_actions + ">" + _params + "</button>";
}

/**
 *
 * @returns {string}
 */
export function loading() {
    /*document.addEventListener("DOMContentLoaded", function (event) {
        //ready
        console.log(event);
        var elemento = document.querySelector('.loadding').remove();
    });*/

    return "<div class='loadding' id='loadding'>" +
    "<div class=\"spinner-border text-light\" role=\"status\">\n" +
    "  <span class=\"sr-only\">Loading...</span>\n" +
    "</div></div>";


}

export function loadingRemove() {
    var elemento = document.querySelector('#loadding');
    console.log(elemento)
    elemento.style.display = 'none';

}

export function fetchGet(url, _target, _jsonR, _structure = null) {
    
    /*var promise = new Promise(function(resolve, reject) {
        var str = "";
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
          resolve(this.responseText);
          str = this.responseText;
          return str;

        };
        xhr.onerror = reject;
        xhr.open('GET', url, true);
        xhr.send(null);
        
    }); 
    */

    /*const http = new XMLHttpRequest();
        http.onload = function() {
            var data = JSON.parse(this.responseText);
            return data;
        }
        http.open('GET', url, true);
        http.send(null);*/
        // return data;
    fetch(url)
    .then(function(response) {
        return response.json();
    })
    .then(function(jsonResponse) {
        // do something with jsonResponse

        // return jsonResponse;
        let json = "";
        let json_pars = jsonResponse.results; //jsonResponse.results;
        
        console.log([jsonResponse, _structure, json_pars]);

        for(let i = 0; i < json_pars.length; i++) {
            json += '<div class="col-6 text-center mb-1">';
            json += '<img class="img-fluid" src="' +json_pars[i].media[0].tinygif.url +'" width="' +json_pars[i].media[0].gif.dims[0] +'" height="' +json_pars[i].media[0].gif.dims[1] +'">';
            json += '</div>';

        }
        json += '<div class="col-6 text-center mb-1">';
        json += '<button class="mdc-button mdc">';
            json += '<span class="mdc-text-field__ripple"></span>';
            json += '<i class="material-icons">add</i>';
            json += '<span class="mdc-button__label">Mais</i>';
        json += '</button>';
        json += '</div>';
        /*json += '<div class="col-6 text-center mb-1">';
        json += '<button class="mdc-button mdc">';
            json += '<span class="mdc-text-field__ripple"></span>';
            json += '<i class="material-icons">add</i>';
            json += '<span class="mdc-button__label">Mais</i>';
        json += '</button>';
        json += '</div>';*/
        console.log(json);
        document.querySelector(_target).innerHTML = json;
    }).catch(function(error){
        console.log(error);
    })
}

/**
 *
 * @param params
 * @param tl_increment
 * @returns {string}
 */
export function new_component_for(params = Array(), tl_increment = null, callback = null) {
    let par = Array();

    
    if(callback == null) {
        for (var i = 0; i < tl_increment; i++) {
            par.push(params[i]);
        }
        return JSON.stringify(par);
    }else {
        for (var i = 0; i < tl_increment; i++) {
            par.push(params[i]);
        }
        return par;

    }
}

/**
 *
 * @param id
 * @param _type
 * @param _params
 * @param _class
 * @param event
 * @returns {string}
 */
export function new_component_tab(id, _type, _params = Array(), _class = null, event = null) {
    let li = "";
    for (let i = 0; i < _params.length; i++) {

        li += "<button type=\"" + _type + "\" class=\"" + _class + "\" " + event + " id=\"" + id + "\">" + _params[i] + "</button>";
    }
    return li;
}

export function new_component_main(_params = Array(), _class = null) {
    let li = "<div class=\"mdc-top-app-bar--fixed-adjust " + _class + "\">";

    for (let i = 0; i < _params.length; i++) {
        li += _params[i];
    }
    li += "</div>";
    return li;
}

export function new_component_menu_header(_params = Array(), _class = null) {

    let header = "<div class=\"mdc-drawer__header\">";
    /*<h3 className="mdc-drawer__title">Mail</h3>
    <h6 className="mdc-drawer__subtitle">email@material.io</h6>*/
    for (let i = 0; i < _params.length; i++) {
        header += _params[i];
    }
    header += "</div>";
    // document.getElementsByClassName('mdc-drawer').innerHTML += header;
    return header;
}

export function new_component_menu(_params = Array(), _class = null, _icons = null) {
    // console.log(_params);
    let li = "<aside class=\"mdc-drawer " + _class + "\">";
    li += "    <div class=\"mdc-drawer__content\">";
    li += "        <nav class=\"mdc-list\">";


    for (let i = 0; i < _params.length; i++) {
        li += _params[i];

    }
    li += "        </nav>";
    li += "    </div>";
    li += "</aside>";
    li += "<div class=\"mdc-drawer-scrim\"></div>";
    // return li;
    document.getElementById('app').innerHTML += li;
};
export function getRouteOnclick(_route) {
    return "location.href='?route="+_route+"'";
}
/**
 *
 * @type {{inputs: (function ( * =, * =, *=): (void|*|undefined))}}
 */
// validate_inputs = {

//     /**
//      *
//      * @param form_id
//      * @param params
//      * @param callBack
//      * @returns {void|*}
//      */
//     inputs: function (form_id, params = Array(), callBack = []) {
//         console.log(callBack)
//         let res = true;
//         const form = document.getElementById(form_id);
//         const input = form.querySelectorAll('input');
//         for (let j = 0; j < params.length; j++) {
//             for (let i = 0; i < input.length; i++) {

//                 if (input[i].getAttribute(params[j]) != null) {
//                     if (input[i].value === "") {

//                         let rq = input[i].getAttribute('required');
//                         create_alert.error(input[i].nodeName, input[i].getAttribute('name') + " is: " + rq)
//                         res = false;

//                     }
//                 }

//             }
//         }
//         if (res) {
//             return create_ajax.method(callBack[0], callBack[1], callBack[2]);
//         }


//     }
// }
// /**
//  *
//  * @type {{method: create_ajax.method}}
//  */
// create_ajax = {
//     /**
//      *
//      * @param method
//      * @param url
//      * @param callback
//      */
//     method: function (method, url, callback) {
//         if (url === "") {
//             create_alert.error('URL', 'Parametro url não foi passado na chamada da função')
//         }
//         jQuery.ajax

//         let http = new XMLHttpRequest();
//         http.overrideMimeType("application/json");

//         http.open(method, url, true);
//         http.onreadystatechange = () => {
//             let json = "";
//             if (http.readyState == 4) {
//                 if (http.status == 200) {

//                     callback(http.responseText);
//                 }
//             }
//         };
//         http.send();

//     }
// }
// /**
//  *
//  * @type {{
//  * alert_all_error: create_alert.alert_all_error,
//  * alertDismissed: create_alert.alertDismissed, error: create_alert.error}}
//  */
// create_alert = {
//     /**
//      *
//      */
//     alert_all_error: function () {
//         const stack = new Error();
//         if (stack.name !== null) {
//             this.error(stack.name, stack.stack);
//         }
//     },
//     /**
//      *
//      * @param error_type
//      * @param msg
//      */
//     error: function (error_type, msg) {
//         let li = "<div class='error' style=' padding: 15px; background: rgb(0,0,0); color: #fff !important; word-wrap: break-word; display: block'>";
//         // li += "<pre style='color: #fff !important'>";
//         li += "<span style='color: #08aff1; padding: 3px; margin-top: 3px; border-radius: 6px;'>" + error_type + ":</span><br>";
//         li += "<span style='color: #929292; padding: 3px; margin-top: 3px; border-radius: 6px;'>======================</span><br>";
//         li += "<span style='color: chartreuse; padding: 3px; margin-top: 3px; border-radius: 6px;'>" + "</span>" + msg + "<br>";
//         li += "<span style='color: #929292; padding: 3px; margin-top: 3px; border-radius: 6px;'>======================</span>";
//         // li += "</pre>";
//         li += "</div>";
//         // navigator.notification.alert(
//         //     'You are the winner!',  // message
//         //     create_alert.alertDismissed(),         // callback
//         //     'Game Over',            // title
//         //     'Done'                  // buttonName
//         // );
//         document.write(li);

//     },
//     alertDismissed: function () {

//     }

// }
// /**
//  *
//  * @type {{obj_tabs: open_tabs.obj_tabs}}
//  */
// open_tabs = {
//     /**
//      *
//      * @param id
//      */
//     obj_tabs: function (id) {
//         let obj = document.getElementById(id);
//         obj_query = obj.querySelectorAll('button');
//         for (let i = 0; i < obj_query.length; i++) {
//             obj_query[i].style.left = i * 19.5 + "%";
//             obj_query[i].style.transition = " ease-in 0.5s";
//         }
//         let obj_page = document.querySelector('.component-tab');
//         let obj_btn_pages = document.querySelector('.btn-tabs');

//         obj_btn_pages.style.backgroundColor = 'rgba(200,200,200,0.5)';
//         obj_btn_pages.style.width = "100%";
//         obj_btn_pages.style.height = "100%";
//         obj_btn_pages.style.left = "none";

//         obj_btn_pages.style.borderRadius = "0";
//         obj_btn_pages.style.transition = "ease-in 0.5s";


//         obj_page.setAttribute('onclick', 'close_tabs.obj_tabs("' + id + '")')
//         obj_page.innerHTML = create_components.new_component_text('span', 'close', 'material-icons-outlined')


//     }
// }
// /**
//  *
//  * @type {{obj_tabs: close_tabs.obj_tabs}}
//  */
// close_tabs = {
//     /**
//      *
//      * @param id
//      */
//     obj_tabs: function (id) {
//         let obj = document.getElementById(id);
//         obj_query = obj.querySelectorAll('button');
//         for (let i = 0; i < obj_query.length; i++) {
//             obj_query[i].style.right = "5%";
//             obj_query[i].style.left = "82.5%";
//             obj_query[i].style.transition = " ease-in 0.5s";
//         }
//         let obj_page = document.querySelector('.component-tab');
//         let obj_btn_pages = document.querySelector('.btn-tabs');

//         obj_btn_pages.style.backgroundColor = 'rgba(255,255,255,0.5)';
//         obj_btn_pages.style.bottom = "0";
//         obj_btn_pages.style.height = "0";
//         obj_btn_pages.style.right = "0";
//         obj_btn_pages.style.width = "0";
//         obj_btn_pages.style.borderRadius = "100px";
//         obj_btn_pages.style.transition = "ease-in 0.7s";

//         obj_page.setAttribute('onclick', 'open_tabs.obj_tabs("' + id + '")')

//         obj_page.innerHTML = create_components.new_component_text('span', 'add', 'material-icons-outlined')


//     }
// }
