import { header, title, links, metatags } from "./modules/defaults_links/defaults_header.js";
import { scripts_default } from "./modules/defaults_scripts/default_scripts.js";
import {getUrlParametros, routes } from "./modules/init_pages/init_pages.js";


header([
    title(['Como Usar Material Design']),
    metatags(['charset="utf-8"', "name=\"viewport\" content=\"width=device-width, initial-scale=1.0\""]),
    links([
        "https://unpkg.com/material-components-web@latest/dist/material-components-web.min.css",
        "https://fonts.googleapis.com/icon?family=Material+Icons|Material+Icons+Outlined|Material+Icons+Two+Tone|Material+Icons+Round|Material+Icons+Sharp",
        "https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;500;700;900&display=swap",
        "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
        "https://fonts.googleapis.com/css2?family=Marvel:ital,wght@0,400;0,700;1,400;1,700&display=swap",
        "https://fonts.googleapis.com/css2?family=Oswald:wght@200;300;400;500;600;700&display=swap",
        "css/default.css",
        "css/marvel.css"
    ])
])
scripts_default(
    [
        "https://code.jquery.com/jquery-3.3.1.slim.min.js",
        "https://unpkg.com/material-components-web@latest/dist/material-components-web.min.js",
        "https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js",
        "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js",
        // "https://cdnjs.cloudflare.com/ajax/libs/crypto-js/3.1.9-1/md5.js",
        // "js/functions.js"
    ]
)
routes(getUrlParametros());
